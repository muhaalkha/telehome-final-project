import React, { useEffect } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as Font from "expo-font";
import * as firebase from "firebase";
import { StatusBar } from "expo-status-bar";

const fetchFonts = () => {
  return Font.loadAsync({
    "Poppins-Black": require("../../../assets/fonts/Poppins-Black.ttf"),
    "Poppins-Bold": require("../../../assets/fonts/Poppins-Bold.ttf"),
    "Poppins-Regular": require("../../../assets/fonts/Poppins-Regular.ttf"),
  });
};

export default function About({ navigation }) {
  useEffect(() => {
    fetchFonts();
  }, []);

  const logOut = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        //console.log("Berhasil Keluar");
        navigation.navigate("Login");
      });
  };

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#4662FE", "#2C48E6", "#021FBF"]}
        style={styles.background}
        start={{ x: 0.7, y: 0 }}
      >
        <View style={styles.card}>
          <Image
            source={require("../images/LRM_EXPORT_251238373775520_20181230_160011559_2_1.jpg")}
            style={styles.avatar}
          />
          <Text style={styles.namaSaya}>M. Habib Al Khairi</Text>
          <Text style={styles.profesi}>Belajar React Native</Text>
          <View style={styles.containerIkon}>
            <Image
              style={styles.ikon}
              source={require("../images/uil_instagram.png")}
            />
            <Image
              style={styles.ikon}
              source={require("../images/uil_facebook.png")}
            />
            <Image
              style={{ width: 23, height: 23 - 23 * 0.12, marginHorizontal: 7 }}
              source={require("../images/uil_gitlab.png")}
            />
            <Image
              style={styles.ikon}
              source={require("../images/uil_linkedin.png")}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={logOut}
          activeOpacity={0.8}
          style={styles.globalButton}
        >
          <Text style={styles.buttonText}>Keluar</Text>
        </TouchableOpacity>
      </LinearGradient>
      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    padding: 27,
  },
  titleText: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#383838",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  globalButton: {
    width: 306,
    height: 50,
    marginTop: 20,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#3652EF",
  },
  textInputLabel: {
    fontSize: 14,
    color: "white",
    marginTop: 10,
    textAlign: "left",
  },
  card: {
    width: 306,
    height: 270,
    backgroundColor: "white",
    marginTop: 15,
    borderRadius: 24,
    padding: 28,
    alignItems: "center",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 60,
  },
  namaSaya: {
    fontSize: 21,
    fontFamily: "Poppins-Bold",
    marginTop: 12,
    color: "#3652EF",
  },
  profesi: {
    fontFamily: "Roboto",
    fontSize: 13,
    color: "#3652EF",
  },
  ikon: {
    width: 23,
    height: 23,
    marginHorizontal: 7,
  },
  containerIkon: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});
