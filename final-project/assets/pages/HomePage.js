import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import * as firebase from "firebase";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import * as Font from "expo-font";
import { StatusBar } from "expo-status-bar";

const fetchFonts = () => {
  return Font.loadAsync({
    "Poppins-Black": require("../../../assets/fonts/Poppins-Black.ttf"),
    "Poppins-Bold": require("../../../assets/fonts/Poppins-Bold.ttf"),
    "Poppins-Regular": require("../../../assets/fonts/Poppins-Regular.ttf"),
  });
};

export default function HomePage({ navigation }) {
  const [sensorData, setSensorData] = useState({});

  useEffect(() => {
    getData();
    fetchFonts();
  }, []);

  const getData = () => {
    //Fungsi read data
    const DHT11 = firebase
      .database()
      .ref("DHT11")
      .on("value", (snapshot) => {
        const data = snapshot.val();
        setSensorData(data);
      });
  };

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#4662FE", "#2C48E6", "#021FBF"]}
        style={styles.background}
        start={{ x: 0.7, y: 0 }}
      >
        <View
          style={{
            width: "100%",
          }}
        >
          <Image
            style={styles.avatar}
            source={require("../images/LRM_EXPORT_251238373775520_20181230_160011559_2_1.jpg")}
          />
        </View>
        <View style={{ width: "100%" }}>
          <Text style={styles.greetingsText}>Hello, Habib</Text>
          <Text style={styles.selamaDatang}>Selamat Datang di Rumah</Text>
        </View>

        <View style={styles.cardContainer}>
          {/********* Masuk Kamar *********/}
          <TouchableOpacity
            onPress={() => navigation.navigate("Kamar")}
            activeOpacity={0.8}
            style={styles.cardList}
          >
            <View>
              <Text style={styles.cardTitle}>Kamar</Text>
              <Text style={styles.jumlahPerangkat}>3 Perangkat</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "flex-end",
                justifyContent: "space-between",
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/Lampu.png")}
                />
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/Televisi.png")}
                />
              </View>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={{ width: 22, height: 22 }}
                  source={require("../images/Group4.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
          {/********* Masuk Ruang Tamu *********/}
          <TouchableOpacity
            // onPress={() => console.log("Masuk Ruang Tamu")}
            activeOpacity={0.8}
            style={styles.cardList}
          >
            <View>
              <Text style={styles.cardTitle}>Ruang Tamu</Text>
              <Text style={styles.jumlahPerangkat}>3 Perangkat</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "flex-end",
                justifyContent: "space-between",
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/Lampu.png")}
                />
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/Televisi.png")}
                />
              </View>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={{ width: 22, height: 22 }}
                  source={require("../images/Group4.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.cardContainer}>
          {/********* Masuk Garasi *********/}

          <TouchableOpacity
            // onPress={() => console.log("Masuk Garasi")}
            activeOpacity={0.8}
            style={styles.cardList}
          >
            <View>
              <Text style={styles.cardTitle}>Garasi</Text>
              <Text style={styles.jumlahPerangkat}>2 Perangkat</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "flex-end",
                justifyContent: "space-between",
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/Lampu.png")}
                />
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/Colokan.png")}
                />
              </View>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={{ width: 22, height: 22 }}
                  source={require("../images/Group4.png")}
                />
              </View>
            </View>
          </TouchableOpacity>

          {/********* Masuk Dapur *********/}

          <TouchableOpacity
            // onPress={() => console.log("Masuk Dapur")}
            activeOpacity={0.8}
            style={styles.cardList}
          >
            <View>
              <Text style={styles.cardTitle}>Dapur</Text>
              <Text style={styles.jumlahPerangkat}>2 Perangkat</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "flex-end",
                justifyContent: "space-between",
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/gg_smart-home-refrigerator.png")}
                />
                <Image
                  style={styles.devicesIcon}
                  source={require("../images/Lampu.png")}
                />
              </View>
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={{ width: 22, height: 22 }}
                  source={require("../images/Group4.png")}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </LinearGradient>
      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    padding: 26,
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  globalButton: {
    width: 306,
    height: 50,
    marginTop: 20,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#3652EF",
  },
  textInputLabel: {
    fontSize: 14,
    color: "white",
    marginTop: 10,
    textAlign: "left",
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginTop: 20,
    borderColor: "white",
    borderWidth: 2,
  },
  greetingsText: {
    fontSize: 25,
    color: "white",
    textAlign: "left",
    marginTop: 10,
    fontFamily: "Poppins-Bold",
  },
  selamaDatang: {
    fontSize: 12,
    color: "white",
    textAlign: "left",
    fontFamily: "Roboto",
    marginBottom: 12,
  },
  cardList: {
    width: 147,
    height: 150,
    backgroundColor: "white",
    borderRadius: 15,
    elevation: 10,
    padding: 20,
  },
  cardContainer: {
    width: "100%",
    marginTop: 14,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardTitle: {
    fontSize: 16,
    fontFamily: "Poppins-Bold",
    color: "#383838",
  },
  jumlahPerangkat: {
    fontSize: 12,
    fontFamily: "Roboto",
    marginTop: 8,
  },
  devicesIcon: { width: 20, height: 20 },
});
