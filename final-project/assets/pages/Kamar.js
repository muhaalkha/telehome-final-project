import React, { useState, useEffect } from "react";
import { Switch, Image, StyleSheet, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as firebase from "firebase";
import * as Font from "expo-font";
import { StatusBar } from "expo-status-bar";

const fetchFonts = () => {
  return Font.loadAsync({
    "Poppins-Black": require("../../../assets/fonts/Poppins-Black.ttf"),
    "Poppins-Bold": require("../../../assets/fonts/Poppins-Bold.ttf"),
    "Poppins-Regular": require("../../../assets/fonts/Poppins-Regular.ttf"),
  });
};

export default function Kamar({ navigation }) {
  const [sensorData, setSensorData] = useState({});
  const [lampu, setLampu] = useState(false);
  const [kondisiLampu, setKondisiLampu] = useState("");

  useEffect(() => {
    fetchFonts();
    getData();
    getKondisiLampu();
  }, []);

  const toggleSwitch = (value) => {
    setLampu(value);
    updateLampuValue(value);
  };

  const updateLampuValue = (value) => {
    //Fungsi update
    firebase
      .database()
      .ref("Devices")
      .update({ Lampu: Number(value) });
  };

  const getData = () => {
    //Fungsi read data
    const DHT11 = firebase
      .database()
      .ref("DHT11")
      .on("value", (snapshot) => {
        const data = snapshot.val();
        setSensorData(data);
      });
  };
  const getKondisiLampu = () => {
    //Fungsi read data
    const DHT11 = firebase
      .database()
      .ref("Devices")
      .on("value", (snapshot) => {
        const data = snapshot.val();
        if (data.statusLampu == 1) {
          setKondisiLampu("Hidup");
        } else {
          setKondisiLampu("Mati");
        }
        //console.log(data);
      });
  };

  return (
    <View style={styles.container}>
      {/******** Header ********/}
      <LinearGradient
        colors={["#4662FE", "#2C48E6", "#021FBF"]}
        style={styles.background}
        start={{ x: 0.7, y: 0 }}
      >
        <View style={styles.backContainer}>
          {/******** Tombol Back ********/}
          <TouchableOpacity
            onPress={() => navigation.navigate("MainPage")}
            style={{ flexDirection: "row", alignItems: "center" }}
          >
            <Image
              style={styles.backButton}
              source={require("../images/Back.png")}
            />
            <Text style={styles.backTitle}>Kamar</Text>
          </TouchableOpacity>
        </View>
        {/******** Header Title ********/}
        <Text style={styles.headerTitle}>Kamar</Text>
        {/******** Temperatur ********/}
        <View style={{ flexDirection: "row", marginTop: 24 }}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Image
              style={styles.sensorIkon}
              source={require("../images/Temperatur.png")}
            />
            <View style={{ marginLeft: 10, justifyContent: "center" }}>
              <Text style={styles.dataText}>{`${sensorData.Temp}°C`}</Text>
              <Text style={{ color: "white", fontFamily: "Roboto" }}>
                Temperatur
              </Text>
            </View>
          </View>
          {/******** Kelembaban ********/}
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Image
              style={styles.sensorIkon}
              source={require("../images/Kelembaban.png")}
            />
            <View style={{ marginLeft: 10, justifyContent: "center" }}>
              <Text style={styles.dataText}>{`${sensorData.Kelembaban}%`}</Text>
              <Text style={{ color: "white", fontFamily: "Roboto" }}>
                Kelembaban
              </Text>
            </View>
          </View>
        </View>
      </LinearGradient>
      <View style={{ padding: 27 }}>
        <View style={styles.daftarDevices}>
          <View style={styles.cardWrapper}>
            <Image
              source={require("../images/Lampu.png")}
              style={styles.ikonDevices}
            />
            <View>
              <Text style={styles.devicesName}>Lampu</Text>
              <Text style={styles.statusDevices}>{kondisiLampu}</Text>
            </View>
          </View>
          <View style={styles.switch}>
            <Switch
              trackColor={{ false: "#E5E5E5", true: "#65C3D8" }}
              thumbColor={"#4662FE"}
              onValueChange={toggleSwitch}
              value={lampu}
            />
          </View>
        </View>
        <View style={styles.daftarDevices}>
          <View style={styles.cardWrapper}>
            <Image
              source={require("../images/Lampu.png")}
              style={styles.ikonDevices}
            />
            <View>
              <Text style={styles.devicesName}>Lampu</Text>
              <Text style={styles.statusDevices}>Mati</Text>
            </View>
          </View>
          <View style={styles.switch}>
            <Switch
              disabled={true}
              trackColor={{ false: "#E5E5E5", true: "#65C3D8" }}
              thumbColor={"#C6C6C6"}
            />
          </View>
        </View>
        <View style={styles.daftarDevices}>
          <View style={styles.cardWrapper}>
            <Image
              source={require("../images/Televisi.png")}
              style={styles.ikonDevices}
            />
            <View>
              <Text style={styles.devicesName}>Televisi</Text>
              <Text style={styles.statusDevices}>Mati</Text>
            </View>
          </View>
          <View style={styles.switch}>
            <Switch
              disabled={true}
              trackColor={{ false: "#E5E5E5", true: "#65C3D8" }}
              thumbColor={"#C6C6C6"}
            />
          </View>
        </View>
      </View>
      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    height: 260,
    width: "100%",
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
    padding: 27,
  },

  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F8F8F8",
  },
  backButton: {
    height: 15,
    width: 15,
    marginTop: 20,
  },
  backContainer: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
  },
  backTitle: {
    fontFamily: "Roboto",
    fontSize: 16,
    marginTop: 20,
    marginLeft: 20,
    fontWeight: "bold",
    color: "white",
  },
  headerTitle: {
    fontSize: 30,
    fontFamily: "Poppins-Bold",
    marginTop: 33,
    color: "white",
  },
  sensorIkon: {
    height: 50,
    width: 50,
  },
  dataText: {
    fontSize: 17,
    fontFamily: "Roboto",
    color: "white",
    fontWeight: "bold",
  },
  daftarDevices: {
    width: 306,
    height: 82,
    backgroundColor: "white",
    elevation: 10,
    borderRadius: 15,
    marginBottom: 14,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  ikonDevices: {
    width: 40,
    height: 40,
    marginLeft: 16,
  },
  cardWrapper: {
    flexDirection: "row",
  },
  devicesName: {
    fontSize: 14,
    fontFamily: "Poppins-Bold",
    marginLeft: 11,
    color: "#383838",
  },
  statusDevices: {
    fontSize: 12,
    marginLeft: 11,
    color: "#383838",
  },
  switch: {
    flexDirection: "row",
    marginRight: 14,
  },
});
