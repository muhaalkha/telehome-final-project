import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import * as firebase from "firebase";
import { LinearGradient } from "expo-linear-gradient";
import * as Font from "expo-font";
import { StatusBar } from "expo-status-bar";

const fetchFonts = () => {
  return Font.loadAsync({
    "Poppins-Black": require("../../../assets/fonts/Poppins-Black.ttf"),
    "Poppins-Bold": require("../../../assets/fonts/Poppins-Bold.ttf"),
    "Poppins-Regular": require("../../../assets/fonts/Poppins-Regular.ttf"),
  });
};

export default function Register({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    fetchFonts();
  }, []);

  const firebaseConfig = {
    apiKey: "AIzaSyA8zy0-p85MXSA8CbGGYtX20zKIJf4n3Xw",
    authDomain: "final-project-sanbercode-dd5b6.firebaseapp.com",
    projectId: "final-project-sanbercode-dd5b6",
    storageBucket: "final-project-sanbercode-dd5b6.appspot.com",
    messagingSenderId: "578764329367",
    appId: "1:578764329367:web:f395c37f1555ee65c4f4d2",
  };
  // Initialize Firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  const submit = () => {
    const data = {
      email,
      password,
    };
    //console.log(data);
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        navigation.navigate("Login");
        //console.log("Reg Berhasil");
      })
      .catch(() => {
        //console.log("Reg Gagal");
      });
  };
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#4662FE", "#2C48E6", "#021FBF"]}
        style={styles.background}
        start={{ x: 0.7, y: 0 }}
      >
        <Image
          style={styles.logoAplikasi}
          source={require("../images/Logo.png")}
        />
        <View style={{ width: 300 }}>
          <Text style={styles.textInputLabel}>Email</Text>
        </View>
        <TextInput
          //placeholder="Email"
          style={styles.inputText}
          value={email}
          onChangeText={(value) => setEmail(value)}
        />
        <View style={{ width: 300 }}>
          <Text style={styles.textInputLabel}>Password</Text>
        </View>
        <TextInput
          //placeholder="Password"
          style={styles.inputText}
          value={password}
          onChangeText={(value) => setPassword(value)}
        />
        <TouchableOpacity
          onPress={submit}
          activeOpacity={0.8}
          style={styles.globalButton}
        >
          <Text style={styles.buttonText}>Register</Text>
        </TouchableOpacity>
        <View style={{ marginTop: 20, flexDirection: "row" }}>
          <Text style={{ color: "white" }}>Sudah punya akun? </Text>
          <Text
            onPress={() => navigation.navigate("Login")}
            style={{ color: "white", fontWeight: "bold" }}
          >
            Login
          </Text>
        </View>
      </LinearGradient>
      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
    alignItems: "center",
  },
  titleText: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#383838",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  inputText: {
    width: 300,
    height: 50,
    paddingLeft: 20,
    marginTop: 10,
    borderWidth: 1,
    borderColor: "white",
    color: "white",
    borderRadius: 25,
  },
  globalButton: {
    width: 300,
    height: 50,
    marginTop: 20,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#3652EF",
  },
  logoAplikasi: {
    width: 156,
    height: 114,
    marginTop: 80,
    marginBottom: 50,
  },
  textInputLabel: {
    fontSize: 14,
    color: "white",
    marginTop: 10,
    textAlign: "left",
  },
});
