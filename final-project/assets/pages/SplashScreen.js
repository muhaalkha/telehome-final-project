import React, { useEffect } from "react";
import { StyleSheet, View, Image } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";

const Splash = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate("Login");
    }, 3000);
  }, []);

  return (
    <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
      <LinearGradient
        colors={["#4662FE", "#2C48E6", "#021FBF"]}
        style={styles.background}
        start={{ x: 0.7, y: 0 }}
      >
        <Image
          style={styles.logoAplikasi}
          source={require("../images/Logo.png")}
        />
      </LinearGradient>
      <StatusBar style="light" />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  tinyLogo: {
    width: 50,
    height: 50,
  },
  background: {
    flex: 1,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  logoAplikasi: {
    width: 156,
    height: 114,
    marginTop: 80,
    marginBottom: 50,
  },
});
