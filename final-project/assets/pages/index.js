import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import "react-native-gesture-handler";
import Kamar from "./Kamar";
import HomePage from "./HomePage";
import Login from "./Login";
import Register from "./Register";
import Icon from "react-native-vector-icons/Feather";
import About from "./About";
import { Dimensions } from "react-native";
import Splash from "./SplashScreen";

const DEVICE_WIDTH = Dimensions.get("window").width;
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Splash"
          component={Splash}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Login"
          component={Login}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Register"
          component={Register}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="MainPage"
          component={MainApp}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Kamar"
          component={Kamar}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const MainApp = () => (
  <Tab.Navigator
    tabBarOptions={{
      activeTintColor: "#3652EF",
      style: {
        borderTopLeftRadius: 21,
        borderTopRightRadius: 21,
        backgroundColor: "white",
        position: "absolute",
        bottom: 0,
        padding: 10,
        width: DEVICE_WIDTH,
        height: 54,
        zIndex: 8,
      },
    }}
  >
    <Tab.Screen
      options={{
        headerShown: false,
        tabBarLabel: "Home",
        tabBarIcon: ({ color, size }) => (
          <Icon name="home" size={size} color={color} />
        ),
      }}
      name="HomeScreen"
      component={HomePage}
    />
    <Tab.Screen
      options={{
        headerShown: false,
        tabBarLabel: "Tentang",
        tabBarIcon: ({ color, size }) => (
          <Icon name="user" size={size} color={color} />
        ),
      }}
      name="About"
      component={About}
    />
  </Tab.Navigator>
);
const styles = StyleSheet.create({});
